import { SetMetadata } from "@nestjs/common";
import { ProtoUserRoleEnum } from "../contracts/auth/types/auth.types";

export const ROLE_KEY = Symbol('role');
export const SetRole = (role: ProtoUserRoleEnum) => SetMetadata(ROLE_KEY, role);