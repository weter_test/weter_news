import { ProtoUserRoleEnum } from "@core/contracts/auth/types/auth.types";
import { IAuthController } from "@core/contracts/auth/types/rpc.types";
import { ROLE_KEY } from "@core/decorators/setRole.decorator";
import { AUTH_MICROSERVICE_NAME } from "@infrastructure/microservices/config/authMicroservice.config";
import { Inject, Injectable } from "@nestjs/common/decorators";
import { HttpStatus } from "@nestjs/common/enums";
import { HttpException } from "@nestjs/common/exceptions";
import { CanActivate, ExecutionContext, OnModuleInit } from "@nestjs/common/interfaces";
import { Reflector } from "@nestjs/core";
import { ClientGrpc } from "@nestjs/microservices";
import { Request } from "express";
import { firstValueFrom } from "rxjs";

@Injectable()
export class AuthRoleGuard implements CanActivate, OnModuleInit {

	authController: IAuthController

	constructor(
		@Inject(AUTH_MICROSERVICE_NAME) private readonly client: ClientGrpc,
		private readonly reflector: Reflector,
	) {}

	onModuleInit() {
	  	this.authController = this.client.getService<IAuthController>('AuthController');
	}

	async canActivate(context: ExecutionContext): Promise<boolean> {
		try {
			const request = context.switchToHttp().getRequest<Request>();
			const authHeader = request.headers.authorization;
			const [bearer, token] = authHeader.split(' ');
	
			if (bearer !== 'Bearer' || !token) throw new Error()
			
			const role = this.reflector.getAllAndOverride<ProtoUserRoleEnum>(ROLE_KEY, [
				context.getHandler(),
				context.getClass()
			])

			const verified = await firstValueFrom(this.authController.verify({
				token,
				role
			}));
			
			if(!verified.id) throw new Error()

			request.user = verified.id
			return true;
		} catch (error) {
			throw new HttpException('Unauthorized!', HttpStatus.UNAUTHORIZED)
		}
	}
}