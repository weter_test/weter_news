import { Observable } from "rxjs";
import { IProtoUser } from "./user.types";

export interface IUserListRequest {
	users: string[]
}

export interface IUserListResponse {
	users: IProtoUser[]
}

export interface IUserController {
	userList(dto: IUserListRequest): Observable<IUserListResponse>
}