export enum ProtoUserRoleEnum {
	ADMIN = 0,
	USER = 1
}

export interface IAuth {
	id: string
}