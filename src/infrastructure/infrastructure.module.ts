import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MicroservicesModule } from './microservices/microservices.module';
import { PostgresModule } from './postgres/postgres.module';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    MicroservicesModule,
    PostgresModule,
  ],
})
export class InfrastructureModule {}
