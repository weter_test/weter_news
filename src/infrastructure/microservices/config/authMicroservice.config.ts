import { getAllRpcPaths } from "@core/utils/getAllRpcPaths";
import { ConfigService } from "@nestjs/config";
import { GrpcOptions, Transport } from "@nestjs/microservices";

export const AUTH_MICROSERVICE_NAME = Symbol('AUTH') 

export const getAuthMicroservice = async (c: ConfigService): Promise<GrpcOptions> => {
	return {
		transport: Transport.GRPC,
		options: {
			url: c.getOrThrow('AUTH_MICROSERVICE'),
			package: ['auth_rpc', 'user_rpc'],
			protoPath: await getAllRpcPaths()
		}
	}
}