import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule } from '@nestjs/microservices';
import { AUTH_MICROSERVICE_NAME, getAuthMicroservice } from './config/authMicroservice.config';

@Global()
@Module({
  imports: [
		ClientsModule.registerAsync([{
		imports: [ConfigModule],
		inject: [ConfigService],
		useFactory: getAuthMicroservice,
		name: AUTH_MICROSERVICE_NAME
		}]),
	],
	exports: [ClientsModule]
})
export class MicroservicesModule {}