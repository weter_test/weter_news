import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { NewsEntity } from '@domains/news/entities/News.entity';
import { NewsLikesEntity } from '@domains/news/entities/NewsLikes.entity';
import { NewsViewsEntity } from '@domains/news/entities/NewsViews.entity';

export const databaseConfigFactory = async (c: ConfigService): Promise<TypeOrmModuleOptions> => {
	return {
                type: 'postgres',
                host: c.getOrThrow('POSTGRES_HOST'),
                port: c.getOrThrow('POSTGRES_PORT'),
                username: c.getOrThrow('POSTGRES_USERNAME'),
                password: c.getOrThrow('POSTGRES_PASSWORD'),
                database: c.getOrThrow('POSTGRES_DATABASE'),
                logging: ['error'],
                maxQueryExecutionTime: 1000,
                synchronize: true,
                applicationName: c.get('POSTGRES_APPNAME'),
                connectTimeoutMS: 15000,
                entities: [NewsEntity, NewsLikesEntity, NewsViewsEntity],
	}
}
