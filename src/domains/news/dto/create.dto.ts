import { IsNotEmpty, IsString, MaxLength } from 'class-validator'
import { INews } from "../entities/News.entity";

export class CreateNewsDto implements Pick<INews, 'title' | 'text'> {
	@IsString()
	@MaxLength(128)
	@IsNotEmpty()
	title: string;
	
	@IsString()
	@MaxLength(512)
	@IsNotEmpty()
	text: string;
}