import { INews } from "../entities/News.entity";

export class NewsList implements Omit<INews, 'likes' | 'views'> {
	id: string;

	title: string;

	text: string;

	createdAt: Date;

	creatorId: string;

	likesCount: number;

	viewsCount: number;
}

export class NewsListResponseDto {
	news: NewsList[]
	
	count: number
}