import { IProtoUser } from "@core/contracts/user/types/user.types";
import { INews } from "../entities/News.entity";

export class NewsResponseDto implements Omit<INews, 'likes' | 'views'> {

	id: string;

	title: string;

	text: string;

	createdAt: Date;

	creatorId: string;

	likes: IProtoUser[];

	views: IProtoUser[];

}