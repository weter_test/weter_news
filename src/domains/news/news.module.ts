import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { NewsEntity } from "./entities/News.entity";
import { NewsLikesEntity } from "./entities/NewsLikes.entity";
import { NewsViewsEntity } from "./entities/NewsViews.entity";
import { NewsController } from "./news.controller";
import { NewsService } from "./news.service";

@Module({
	imports: [
		TypeOrmModule.forFeature([NewsEntity, NewsLikesEntity, NewsViewsEntity])
	],
	controllers: [NewsController],
	providers: [NewsService]
})
export class NewsModule {}