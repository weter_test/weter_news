import { IUserController } from "@core/contracts/user/types/rpc.types";
import { AUTH_MICROSERVICE_NAME } from "@infrastructure/microservices/config/authMicroservice.config";
import { HttpException, HttpStatus, Inject, Injectable, OnModuleInit } from "@nestjs/common";
import { ClientGrpc } from "@nestjs/microservices";
import { InjectRepository } from "@nestjs/typeorm";
import { firstValueFrom } from "rxjs";
import { Repository } from "typeorm";
import { CreateNewsDto } from "./dto/create.dto";
import { ListQueryDto } from "./dto/listQuery.dto";
import { NewsList, NewsListResponseDto } from "./dto/newsListResponse.dto";
import { NewsResponseDto } from "./dto/newsResponse.dto";
import { NewsEntity } from "./entities/News.entity";
import { NewsLikesEntity } from "./entities/NewsLikes.entity";
import { NewsViewsEntity } from "./entities/NewsViews.entity";

@Injectable()
export class NewsService implements OnModuleInit {

	private userController: IUserController;

	constructor(
		@InjectRepository(NewsEntity)
		private readonly newsEntity: Repository<NewsEntity>,
		@InjectRepository(NewsLikesEntity)
		private readonly newsLikesEntity: Repository<NewsLikesEntity>,
		@InjectRepository(NewsViewsEntity)
		private readonly newsViewsEntity: Repository<NewsViewsEntity>,
		@Inject(AUTH_MICROSERVICE_NAME) private readonly client: ClientGrpc
	) {}
  
	onModuleInit() {
	  this.userController = this.client.getService<IUserController>('UserController');
	}
	
	async getNewsList(query: ListQueryDto): Promise<NewsListResponseDto> {
		const limit = +query.limit || 15
		const offset = +query.page ?? 0
		const news: NewsList[] = await this.newsEntity.query(`
			SELECT 
				DISTINCT news.id, 
				news.title, news.text, news."createdAt", news."creatorId",
				COUNT(news_likes."newsId") OVER(PARTITION BY news.id) "likesCount", 
				COUNT(news_views."newsId") OVER(PARTITION BY news.id) "viewsCount"
			FROM news 
				LEFT JOIN news_likes ON (news.id = news_likes."newsId")
				LEFT JOIN news_views ON (news.id = news_views."newsId")
			ORDER BY news."createdAt" DESC
			LIMIT $2
			OFFSET $1
		`, [offset * limit, limit])

		const count = await this.newsEntity.count()

		return {
			news,
			count
		}
	}

	async getNews(id: string): Promise<NewsResponseDto> {
		const news = await this.newsEntity.createQueryBuilder('news')
		.leftJoinAndSelect('news.views', 'views')
		.leftJoinAndSelect('news.likes', 'likes')
		.where('news.id = :id', {id})
		.getOne()

		const likes = await firstValueFrom(this.userController.userList({users: news.likes.map(item => item.userId)}))
		const views = await firstValueFrom(this.userController.userList({users: news.views.map(item => item.userId)}))

		return {
			...news,
			likes: likes.users || [],
			views: views.users || []
		}
	}

	async like(userId: string, newsId: string) {
		const payload = {
			userId, newsId
		}
		const likeExists = await this.newsLikesEntity.exist({
			where: payload
		})

		if(likeExists) throw new HttpException('Like exists', HttpStatus.BAD_REQUEST)

		const like = this.newsLikesEntity.create()

		this.newsLikesEntity.merge(like, payload)

		return this.newsLikesEntity.save(like)
	}

	async view(userId: string, newsId: string) {
		const payload = {
			userId, newsId
		}
		const viesExists = await this.newsViewsEntity.exist({
			where: payload
		})

		if(viesExists) throw new HttpException('View exists', HttpStatus.BAD_REQUEST)

		const view = this.newsViewsEntity.create()

		this.newsViewsEntity.merge(view, payload)

		return this.newsViewsEntity.save(view)
	}

	create(body: CreateNewsDto, creatorId: string) {
		const news = this.newsEntity.create()

		this.newsEntity.merge(news, {...body, creatorId})

		return this.newsEntity.save(news)
	}
	
}