import { AuthRoleGuard } from "@core/guards/authRoles.guard";
import { Body, Controller, Get, Param, ParseUUIDPipe, Post, Query, Req, UseGuards } from "@nestjs/common";
import { Request } from "express";
import { ProtoUserRoleEnum } from "src/core/contracts/auth/types/auth.types";
import { SetRole } from "src/core/decorators/setRole.decorator";
import { CreateNewsDto } from "./dto/create.dto";
import { ListQueryDto } from "./dto/listQuery.dto";
import { NewsService } from "./news.service";

@Controller('news')
@UseGuards(AuthRoleGuard)
export class NewsController {

	constructor(
		private readonly newsService: NewsService
	) {}

	@Get('list')
	getNewsList(@Query() query: ListQueryDto) {
		return this.newsService.getNewsList(query)
	}

	@Get(':id')
	getNews(@Param('id', new ParseUUIDPipe({version: '4'})) id: string) {
		return this.newsService.getNews(id)
	}

	@Post(':id/like')
	like(@Param('id', new ParseUUIDPipe({version: '4'})) id: string, @Req() request: Request) {
		return this.newsService.like(request.user, id)
	}

	@Post(':id/view')
	view(@Param('id', new ParseUUIDPipe({version: '4'})) id: string, @Req() request: Request) {
		return this.newsService.view(request.user, id)
	}

	@Post()
	@SetRole(ProtoUserRoleEnum.ADMIN)
	createNews(@Body() body: CreateNewsDto, @Req() request: Request) {
		return this.newsService.create(body, request.user)
	}
}