import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { NewsEntity } from "./News.entity";

export interface INewsViews {
	id: string
	userId: string
	newsId: string
}

@Entity({ name: 'news_views' })
export class NewsViewsEntity implements INewsViews {

	@PrimaryGeneratedColumn('uuid')
	id: string

	@Column({type: 'uuid', nullable: false})
	newsId: string
	
	@Column({type: 'uuid', nullable: false})
	userId: string
	
	@ManyToOne(() => NewsEntity, (ne) => ne.id, {
		onDelete: 'CASCADE'
	})
	news: NewsEntity
}