import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { NewsLikesEntity } from "./NewsLikes.entity";
import { NewsViewsEntity } from "./NewsViews.entity";

export interface INews {
	id: string
	title: string
	text: string
	createdAt: Date
	creatorId: string
	likes: NewsLikesEntity[]
	views: NewsViewsEntity[]
}

@Entity({ name: 'news' })
export class NewsEntity implements INews {

	@PrimaryGeneratedColumn('uuid')
	id: string

	@Column({type: 'varchar', length: 128, nullable: false})
	title: string
	
	@Column({type: 'varchar', length: 512, nullable: false})
	text: string

	@CreateDateColumn()
	createdAt: Date

	@Column({type: 'uuid', nullable: false})
	creatorId: string

	@OneToMany(() => NewsLikesEntity, (nl) => nl.news)
	likes: NewsLikesEntity[]

	@OneToMany(() => NewsViewsEntity, (nv) => nv.news)
	views: NewsViewsEntity[]
}