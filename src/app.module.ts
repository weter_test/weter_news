import { Module } from '@nestjs/common';
import { DomainModule } from '@domains/domain.module';
import { InfrastructureModule } from '@infrastructure/infrastructure.module';

@Module({
  imports: [InfrastructureModule, DomainModule],
})
export class AppModule {}
